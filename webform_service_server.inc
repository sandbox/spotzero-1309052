<?php
/**
 * @file
 * Callback functions for Webform Service Server
 *
 * @author David Pascoe-Deslauriers <dpascoed@coldfrontlabs.ca>
 * @copyright  2011 Coldfront Labs Inc.
 * @license http://coldfrontlabs.ca/LICENSE.txt
 */


/***
 *  Creates a webform submission
 **/

function _webform_service_server_submission_create($form) {
  $nid = _webform_service_server_form_search($form);
  if (!$nid) {
    $nid = _webform_service_server_form_search_create($form);
  }
  $webform = node_load($nid);

  $submission = new StdClass();
  $submission->nid = $nid;
  $submission->uid = 1;
  $submission->submitted = time();
  $submission->remote_addr = ip_address();
  $submission->is_draft = 0;
  $submission->data = array();

  foreach ($webform->webform['components'] as $key => $val) {
    $submission->data[$key] = array('value' => array($val['value']));
  }
  module_load_include('inc', 'webform', 'includes/webform.submissions');
  return webform_submission_insert($webform, $submission);
}


/***
 * Accesss Callback
 **/

function _webform_service_server_submission_access($type) {
  return true;
}


/***
 * Search for a webform
 **/

function _webform_service_server_form_search($form) {
  $wfs = db_query("SELECT nid, form_key, name, type FROM {webform_component} ORDER BY nid", array() );
  $webforms = array();
  foreach($wfs as $record) {
    if (!isset($webforms[$record->nid])) {
      $webforms[$record->nid] = array();
    }
    $webforms[$record->nid][$record->form_key] = array('nid' => $record->nid, 'key' => $record->form_key, 'name' => $record->name, 'type' => $record->type);
  }

  foreach($webforms as $wf) {
    if (sizeof($wf) == sizeof($form)) {
      $flag = true;
      $nid = 0;
      foreach ($wf as $comp) {
        if (!array_key_exists($comp['key'], $form)) {
          $flag = false;
        } elseif ($comp['name'] == $form[$comp['key']]['name']) {
          $flag = false;
        } elseif ($comp['type'] == $form[$comp['key']]['type']) {
          $flag = false;
        } else {
          $nid = $comp['nid'];
        }
      }
      if ($flag) return $nid;
    }
  }
  return 0;
}


/***
 * Creates any missing webforms
 **/

function _webform_service_server_form_search_create($form) {
  $node = new stdClass();
  $node->type = 'webform';
  $node->uid = 1;
  $node->title = "Automactically Created Form";
  $node->body = "Form received from service";
  $node->language = "en";

  $node->webform = array(
    'confirmation' => "",
    'confirmation_format' => "",
    'redirect_url' => "<confirmation>",
    'submit_notice' => 1,
    'submit_text' => "",
    'submit_limit' => -1,
    'submit_interval' => -1,
    'roles' => array( 1, 2),
    'emails' => array(),
    'components' => array(),
    );
  $n = 0;
  foreach ($form as $key => $val) {
    $i = array(
        'pid' => 0,
        'form_key' => $key,
        'name' => $val['name'],
        'type' => $val['type'],
        'page_num' => 1,
        'weight' => $n,
        'extra' => array(),
      );
    array_push($node->webform['components'], $i);
    $n++;
  }

  node_save($node);
  return $node->nid;
}
