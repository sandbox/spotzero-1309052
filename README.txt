Webform Service Server
======================

Description
-----------

Webform Service Server is an method for services 3 that works in conjuction with the Webform Service Submit module.

The services method accepts webform service submittion, and searches for a matching webform based on the provided fields.  If a matching webform isn't found, a new form is created.

This module was designed to work with Services 3 (http://drupal.org/project/services).


Installation
------------

Enable the module and grant permissions for services users to call the provided method call.


License
-------

Copyright (C) 2011  David Pascoe-Deslauriers, Coldfront Labs Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
